class Administrative::AdminsController < AdministrativeController
    before_action :set_admin, only: [:show, :edit, :update, :destroy]
  
    def index
      @admins = Admin.all
    end
  
    def show
    end
  
    def new
      @admin = Admin.new
    end
  
    def edit
    end
  
    def create
      @admin = Admin.new(admin_params)
  
      respond_to do |format|
        if @admin.save
          format.html { redirect_to administrative_admins_path, notice: 'Administrador criado com sucesso.' }
          format.json { render :show, status: :created, location: @admin }
        else
          format.html { render :new }
          format.json { render json: @admin.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def update
      respond_to do |format|
        if @admin.update(admin_params)
          format.html { redirect_to administrative_admins_path, notice: 'Administrador atualizado com sucesso.' }
          format.json { render :show, status: :ok, location: @admin }
        else
          format.html { render :edit }
          format.json { render json: @admin.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def destroy
      @admin.destroy
      respond_to do |format|
        format.html { redirect_to administrative_admins_path, notice: 'Administrador excluido com sucesso.' }
        format.json { head :no_content }
      end
    end
  
    private

      def set_admin
        @admin = Admin.find(params[:id])
      end
  
      def admin_params
        params.require(:admin).permit(:email)
      end
  end
  