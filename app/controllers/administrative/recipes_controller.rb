class Administrative::RecipesController < AdministrativeController
    before_action :set_recipe, only: [:show, :edit, :update, :destroy]
  
    def index
      @recipes = Recipe.all
    end
  
    def show
    end
  
    def new
      @recipe = Recipe.new
    end
  
    def edit
    end
  
    def create
      @recipe = Recipe.new(recipe_params)
  
      respond_to do |format|
        if @recipe.save
          format.html { redirect_to administrative_recipes_path, notice: 'Receita criada com sucesso.' }
          format.json { render :show, status: :created, location: @recipe }
        else
          format.html { render :new }
          format.json { render json: @recipe.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def update
      respond_to do |format|
        if @recipe.update(recipe_params)
          format.html { redirect_to administrative_recipes_path, notice: 'Receita atualizada com sucesso.' }
          format.json { render :show, status: :ok, location: @recipe }
        else
          format.html { render :edit }
          format.json { render json: @recipe.errors, status: :unprocessable_entity }
        end
      end
    end
  
    def destroy
      @recipe.destroy
      respond_to do |format|
        format.html { redirect_to administrative_recipes_path, notice: 'Receita excluida com sucesso.' }
        format.json { head :no_content }
      end
    end
  
    private

      def set_recipe
        @recipe = Recipe.find(params[:id])
      end
  
      def recipe_params
        params.require(:recipe).permit(:name, :description, :price,
        ingredients_attributes: Ingredient.attribute_names.map(&:to_sym).push(:_destroy))
      end
  end
  